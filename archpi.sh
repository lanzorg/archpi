#!/usr/bin/env bash

#region Helper Functions

is_dir() {

	local dir="${1}"

	[[ -d "${dir}" ]]

}

is_empty() {

	local var="${1}"

	[[ -z "${var}" ]]

}
 
is_not_empty() {

	local var="${1}"

	[[ -n "${var}" ]]

}
 
is_file() {

	local file="${1}"

	[[ -f "${file}" ]]

}

is_filename() {

	local filename="${1}"

	[[ "${filename}" =~ ^.+\.[A-Za-z0-9]{1,8}$ ]]

}

clear_file() {

    local file="${1}"

    touch "${file}" && cat /dev/null > "${file}"

}

sanitize() {

	local var="${1}"

	var="${var#"${var%%[![:space:]]*}"}"
	var="${var%"${var##*[![:space:]]}"}"
	var=$(echo "${var}" | tr -d '"' | tr -d '\n' | tr -d ' ')

    echo "${var}"

}

filename_from_url() {

	local url="${1}"
	local response
	local filename
	local location

	response=$(curl -A 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0' -sIL "${url}")
	filename=$(echo "${response}" | grep -o -E 'filename=.*$' | sed -e 's/filename=//')
	location=$(echo "${response}" | grep -o -E 'Location:.*$' | sed -e 's/Location://')

	if is_not_empty "${filename}"; then
		filename="${filename}"
	elif is_filename "${url##*/}"; then
		filename="${url##*/}"
	elif is_not_empty "${location}"; then
		filename=$(basename "${location}")
	fi

	sanitize "${filename}"

}

download_from_url() {

	local url="${1}"
	local filepath

	filepath=$(dirname "$(mktemp -u)")/$(filename_from_url "${url}")
	curl -A 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0' -sL "${url}" -o "${filepath}"

	echo "${filepath}"

}

append_line() {

    local file="${1}"
    local line="${2}"

    printf "${line}\n" | tee -a "${file}" >/dev/null

}

install_with_pacman() {

    local pkgs="${@}"

    sudo pacman -S --needed --noconfirm "${pkgs}"

}

install_with_yay() {

    local pkgs="${@}"

    yay -S --needed --noconfirm "${pkgs}"

}

update_system() {

    sudo pacman -Syu --needed --noconfirm

    if [ -x "$(command -v yay)" ]; then
        yay -Syu --needed --noconfirm --nodevel
    fi

}

#endregion Helper Functions

#region Setup Functions

setup_flutter() {

    install_with_yay flutter

    # Ensure that we can use flutter as a normal user.
    sudo groupadd flutterusers
    sudo gpasswd -a "${USER}" flutterusers
    sudo chown -R :flutterusers /opt/flutter
    sudo chmod -R g+w /opt/flutter
    newgrp flutterusers

    # Reload the /etc/environment file.
    source /etc/environment

}

setup_git() {

    local email="${1:-"anonymous@example.com"}"
    local name="${2:-"anonymous"}"
    local timeout="${3:-"3600"}"

    install_with_pacman git

    git config --global user.email "${email}"
    git config --global user.name "${name}"
    git config --global credential.helper "cache --timeout=${timeout}"

}

setup_jdownloader() {

    install_with_yay jdownloader2

}

setup_mpv() {

    local mpv_directory
    local mpv_file
    local input_file
    local shaders_directory
    local urls
    local url
    local shader_file

    install_with_pacman mpv

    mpv_directory="$HOME/.config/mpv"
    mkdir -p "${mpv_directory}"

    mpv_file="${mpv_directory}/mpv.conf"
    clear_file "${mpv_file}"
    append_line "${mpv_file}" 'profile=gpu-hq'
    append_line "${mpv_file}" 'hwdec=auto'
    append_line "${mpv_file}" 'keep-open=yes'

    input_file="${mpv_directory}/input.conf"
    clear_file "${input_file}"
    append_line "${input_file}" 'n cycle-values glsl-shaders "~~/shaders/adaptive-sharpen.glsl" ""'

    shaders_directory="$HOME/.config/mpv/shaders"
    mkdir -p "${shaders_directory}"
    urls=(
        'https://gist.githubusercontent.com/igv/8a77e4eb8276753b54bb94c1c50c317e/raw/7da08a3409d119270cffe99545cbf464073bf20b/adaptive-sharpen.glsl'
        'https://gist.githubusercontent.com/igv/2364ffa6e81540f29cb7ab4c9bc05b6b/raw/44add43ba89236b107431c375e3c98a8841f0065/SSimSuperRes.glsl'
        'https://gist.githubusercontent.com/igv/a015fc885d5c22e6891820ad89555637/raw/c471ef6dcbd3c4a977e1c95dc40944ee38fad08a/KrigBilateral.glsl'
    )
    for url in "${urls[@]}"; do
        shader_file=$(download_from_url "${url}")
        mv "${shader_file}" "${shaders_directory}"
    done

}

setup_nodejs() {

    install_with_pacman nodejs yarn

}

setup_openjdk() {

    install_with_pacman jdk-openjdk

    # Make sure the default JDK is the latest one.
    sudo archlinux-java set $(ls -t /usr/lib/jvm/ | head -n 1)

}

setup_python() {

    install_with_pacman python python-poetry

}

setup_vscode() {

    install_with_yay visual-studio-code-bin

    code --install-extension ms-azuretools.vscode-docker
    code --install-extension ms-python.python
    code --install-extension visualstudioexptteam.vscodeintellicode

}

setup_youtube_dl() {

    install_with_yay youtube-dl-git

}

#endregion Setup Functions

#region Main

main() {

    # update_system
    # setup_git
    # setup_openjdk

    # setup_flutter
    # setup_nodejs
    # setup_python
    # setup_vscode

    # setup_jdownloader
    setup_mpv
    # setup_youtube_dl

}

#endregion Main

main
